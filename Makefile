# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/01/14 14:55:01 by mverdier          #+#    #+#              #
#    Updated: 2018/12/12 15:38:32 by mverdier         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# Colors.

ORANGE =	\033[1;33m   #It is actually Yellow, but i changed yellow to orange.

GREEN =		\033[1;32m

RED =		\033[1;31m

RES =		\033[0m

#------------------------------------------------------------------------------#

ASM =		./asm

TEST =		./test

all: asm test

asm:
	@$(MAKE) -C $(ASM)

test:
	@$(MAKE) -C $(TEST)

clean:
	@$(MAKE) -C $(ASM) clean
	@$(MAKE) -C $(TEST) clean

fclean:
	@$(MAKE) -C $(ASM) fclean
	@$(MAKE) -C $(TEST) fclean

re: fclean
	@$(MAKE) all

#------------------------------------------------------------------------------#
# List of all my optionnals but usefull rules.

git:
	@$(MAKE) -C $(ASM) git
	@$(MAKE) -C $(TEST) git
	git add Makefile
	git status

.PHONY: all clean re fclean git asm test
