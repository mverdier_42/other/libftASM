section .text
	global _ft_bzero

_ft_bzero:
	enter	16, 0
	mov		r8, 0

start:
	cmp		rsi, r8
	je		end
	mov		[rdi + r8], byte 0
	inc		r8
	;inc		rdi
	jmp		start

end:
	leave
	ret
