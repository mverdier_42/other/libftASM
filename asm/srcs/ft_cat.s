%define SYSCALL(x)	0x2000000 | x
%define READ		3
%define WRITE		4
%define STDOUT		1
%define BUF_SIZE	1024

section .bss

buf	resb BUF_SIZE

section .text
	global _ft_cat
	extern _printf

;rdi = file descriptor

_ft_cat:
	enter	16, 0
	lea		rsi, [rel buf]
	push	rdi

read:
	pop		rdi
	mov		rdx, BUF_SIZE
	mov		rax, SYSCALL(READ)
	syscall
	jc		end
	cmp		rax, 0
	je		end
	push	rdi

write:
	mov		rdi, STDOUT
	mov		rdx, rax
	mov		rax, SYSCALL(WRITE)
	syscall
	cmp		rax, 0
	jl		pop_end
	jmp		read

end:
	leave
	ret

pop_end:
	pop		rdi
	jmp		end
