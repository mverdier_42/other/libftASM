section .text
	global _ft_isalnum
	extern _ft_isalpha
	extern _ft_isdigit

_ft_isalnum:
	enter	16, 0
	mov		rax, 0

is_alpha:
	call	_ft_isalpha
	cmp		rax, 1
	je		end

is_digit:
	call	_ft_isdigit

end:
	leave
	ret
