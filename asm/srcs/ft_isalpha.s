section .text
	global _ft_isalpha

_ft_isalpha:
	enter	16, 0
	mov		rax, 0

upper_case:
	cmp		rdi, 65
	jl		end
	cmp		rdi, 90
	jg		lower_case
	mov		rax, 1
	jmp		end

lower_case:
	cmp		rdi, 97
	jl		end
	cmp		rdi, 122
	jg		end
	mov		rax, 1

end:
	leave
	ret
