section .text
	global _ft_isascii

_ft_isascii:
	enter	16, 0
	mov		rax, 0

is_ascii:
	cmp		rdi, 0
	jl		end
	cmp		rdi, 127
	jg		end
	mov		rax, 1

end:
	leave
	ret
