section .text
	global _ft_isdigit

_ft_isdigit:
	enter	16, 0
	mov		rax, 0

digit:
	cmp		rdi, 48
	jl		end
	cmp		rdi, 57
	jg		end
	mov		rax, 1

end:
	leave
	ret
