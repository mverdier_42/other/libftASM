section .text
	global _ft_isprint

_ft_isprint:
	enter	16, 0
	mov		rax, 0

is_print:
	cmp		rdi, 32
	jl		end
	cmp		rdi, 126
	jg		end
	mov		rax, 1

end:
	leave
	ret
