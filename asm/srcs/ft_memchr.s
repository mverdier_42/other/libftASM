section .text
	global _ft_memchr

;rdi = s
;rsi = c
;rdx = n

_ft_memchr:
	enter	16, 0
	push	rdi
	mov		rcx, rdx
	mov		al, sil
	inc		rcx
	repne	scasb
	cmp		rcx, 0
	jle		null
	dec		rdi
	mov		rax, rdi

end:
	pop		rdi
	leave
	ret

null:
	mov		rax, 0
	jmp		end
