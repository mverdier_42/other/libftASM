section .text
	global _ft_memcmp

;rdi = s1
;rsi = s2
;rdx = n

_ft_memcmp:
	enter	16, 0
	mov		rcx, rdx
	inc		rcx
	repe	cmpsb
	jrcxz	no_diff
	jmp		diff

no_diff:
	mov		rax, 0
	jmp		end

diff:
	dec		rdi
	dec		rsi
	xor		r8, r8
	xor		r9, r9
	mov		r8b, [rdi]
	mov		r9b, [rsi]
	sub		r8, r9
	mov		rax, r8

end:
	leave
	ret
