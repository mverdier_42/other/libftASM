section .text
	global _ft_memcpy

;rdi = dst
;rsi = src
;rdx = n

_ft_memcpy:
	enter	16, 0
	push	rdi
	mov		rcx, rdx
	rep		movsb
	pop		rax

end:
	leave
	ret
