section .text
	global _ft_memset

;rdi = void *b
;rsi = int c
;rdx = size_t len

_ft_memset:
	enter	16, 0
	push	rdi
	mov		rax, rsi
	mov		rcx, rdx
	rep		stosb
	pop		rax

end:
	leave
	ret
