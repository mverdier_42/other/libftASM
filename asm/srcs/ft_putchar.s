%define SYSCALL(x)	0x2000000 | x
%define WRITE		4
%define STDOUT		1

section .text
	global _ft_putchar

;rdi = int c :: dil = char c

_ft_putchar:
	enter	16, 0
	mov		r8b, dil
	mov		[rsp - 4], dil			;save rdi addr
	lea		rsi, [rsp - 4]
	mov		rdi, STDOUT
	mov		rdx, 1
	mov		rax, SYSCALL(WRITE)
	syscall
	cmp		rax, 0
	jle		err
	mov		rax, 0
	mov		al, r8b
	jmp		end

err:
	pop		rdi
	mov		rax, -1

end:
	leave
	ret
