%define SYSCALL(x)	0x2000000 | x
%define WRITE		4
%define STDOUT		1

section .data

newline:
	.char db 10

null:
	.string db "(null)", 10
	.len equ $ - null.string

section .text
	global _ft_puts
	extern _ft_strlen

_ft_puts:
	enter	16, 0
	mov		rsi, rdi				;save string

put_string:
	cmp		rsi, 0
	je		put_null
	call	_ft_strlen				;rdi = string
	cmp		rax, 0
	je		put_newline
	mov		rdi, STDOUT
	mov		rdx, rax				;rax = strlen(string)
	mov		rax, SYSCALL(WRITE)
	syscall
	cmp		rax, 0
	jl		err

put_newline:
	lea		rsi, [rel newline.char]
	mov		rdi, STDOUT
	mov		rdx, 1
	mov		rax, SYSCALL(WRITE)
	syscall
	cmp		rax, 0
	jl		err
	jmp		end

put_null:
	mov		rdi, STDOUT
	lea		rsi, [rel null.string]
	mov		rdx, null.len
	mov		rax, SYSCALL(WRITE)
	syscall
	cmp		rax, 0
	jge		end

err:
	mov		rax, -1
	jmp		end

end:
	leave
	ret
