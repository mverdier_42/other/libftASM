section .text
	global _ft_strcat
	extern _ft_strlen
	extern _ft_memcpy

_ft_strcat:
	enter	16, 0
	push	rdi

to_end_dest:
	cmp		byte [rdi], 0
	je		copy
	inc		rdi
	jmp		to_end_dest

copy:
	mov		r8, rdi
	mov		rdi, rsi
	call	_ft_strlen
	mov		rdx, rax
	mov		rdi, r8
	call	_ft_memcpy
	;cmp		byte [rsi], 0
	;je		terminate_string
	;mov		r8, [rsi]
	;mov		[rdi], r8
	;inc		rdi
	;inc		rsi
	;jmp		copy

terminate_string:
	mov		byte [rdi], 0
	pop		rdi
	mov		rax, rdi

end:
	leave
	ret
