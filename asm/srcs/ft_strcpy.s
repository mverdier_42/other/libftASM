section .text
	global _ft_strcpy
	extern _ft_strlen

_ft_strcpy:
	enter	16, 0
	push	rdi
	mov		r8, rdi
	mov		rdi, rsi
	call	_ft_strlen
	mov		rcx, rax
	mov		rdi, r8
	inc		rcx
	rep		movsb
	pop		rax

end:
	leave
	ret
