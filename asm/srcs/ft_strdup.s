section .text
	global _ft_strdup
	extern _malloc
	extern _ft_strlen
	extern _ft_memcpy

;rdi = src

_ft_strdup:
	enter	16, 0
	push	rdi						;save string src
	call	_ft_strlen				;ft_strlen(rdi)
	mov		rdx, rax				;rdx = strlen(src)
	push	rdx						;save src len
	mov		rdi, rax				;prepare malloc size
	inc		rdi						;size = strlen(src) + 1 (for '\0')
	mov		rax, 8					;prepare multiplication
	mul		rdi						;size = len * sizeof(char)
	call	_malloc					;malloc(rdi = size)
	cmp		rax, 0					;check malloc return value
	je		end						;malloc error
	mov		rdi, rax				;move new ptr to rdi
	pop		rdx						;pop len from stack
	pop		rsi						;load string src to rsi
	push	rdx						;save len
	call	_ft_memcpy				;copy rsi to rdi for rdx len
	pop		rdx						;load src len
	mov		byte [rax + rdx], 0		;terminate rdi string

end:
	leave
	ret								;return new ptr
