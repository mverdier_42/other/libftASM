section .text
	global _ft_strlen

_ft_strlen:
	enter	16, 0
	xor		al, al
	xor		rcx, rcx
	push	rdi

count:
	dec		rcx
	repne	scasb
	inc		rcx
	not		rcx
	mov		rax, rcx

end:
	pop		rdi
	leave
	ret
