section .text
	global _ft_strrev
	extern _ft_strlen
	extern _malloc

_ft_strrev:
	enter	16, 0
	;mov		r8, 0
	push	rdi						;save str
	cmp		rdi, 0
	je		err1
	call	_ft_strlen				;ft_strlen(rdi)
	cmp		rax, 0
	je		empty
	mov		rdx, rax				;rdx = strlen(src)
	push	rdx						;save src len
	mov		rdi, rax				;prepare malloc size
	inc		rdi						;size = strlen(src) + 1 (for '\0')
	mov		rax, 8					;prepare multiplication
	mul		rdi						;size = len * sizeof(char)
	call	_malloc					;malloc(rdi = size)
	cmp		rax, 0					;check malloc return value
	je		err2					;malloc error
	pop		rdx						;pop len from stack
	pop		rsi						;load string src to rsi
	push	rax
	dec		rdx

rev:
	cmp		rdx, 0
	jl		null_termiate
	mov		r9, [rsi + rdx]
	mov		[rax], r9
	dec		rdx
	inc		rax
	jmp		rev

err1:
	pop		rsi
	mov		rax, 0
	jmp		end

err2:
	pop		rdx
	pop		rsi
	jmp		end

empty:
	pop		rdi
	mov		rax, rdi
	jmp		end

null_termiate:
	mov		r9, 0
	mov		[rax], r9
	pop		rax

end:
	leave
	ret
