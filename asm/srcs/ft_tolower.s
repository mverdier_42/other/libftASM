section .text
	global _ft_tolower

_ft_tolower:
	enter	16, 0
	mov		rax, rdi

to_lower:
	cmp		rdi, 65
	jl		end
	cmp		rdi, 90
	jg		end
	add		rax, 32

end:
	leave
	ret
