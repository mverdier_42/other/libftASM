section .text
	global _ft_toupper

_ft_toupper:
	enter	16, 0
	mov		rax, rdi

to_upper:
	cmp		rdi, 97
	jl		end
	cmp		rdi, 122
	jg		end
	sub		rax, 32

end:
	leave
	ret
