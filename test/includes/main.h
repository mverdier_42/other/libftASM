/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 18:36:07 by mverdier          #+#    #+#             */
/*   Updated: 2019/01/14 17:17:46 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <ctype.h>
# include <fcntl.h>
# include <unistd.h>
# include "libfts.h"

void	test_bzero(void);
void	test_isalpha(void);
void	test_isdigit(void);
void	test_isalnum(void);
void	test_isascii(void);
void	test_isprint(void);
void	test_toupper(void);
void	test_tolower(void);
void	test_puts(void);
void	test_strlen(void);
void	test_strcat(void);
void	test_memset(void);
void	test_memcpy(void);
void	test_strdup(void);
void	test_cat(void);
void	test_putchar(void);
void	test_strrev(void);
void	test_strcpy(void);
void	test_memchr(void);
void	test_memcmp(void);

#endif
