/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 18:34:17 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/18 17:27:26 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	print_status(char *s1, char *s2)
{
	if (!strcmp(s1, s2))
		printf("OK\n");
	else
		printf("KO\n");
}

void		test_bzero(void)
{
	char	s1[] = "Bonjour";
	char	s2[] = "Bonjour";
	size_t	len = strlen(s1);

	printf("Test ft_bzero -----------------------------------------------\n\n");

	printf("ft_bzero(NULL, 0): ");
	bzero(NULL, 0);
	ft_bzero(NULL, 0);
	printf("OK\n");

	printf("ft_bzero(s, 0): ");
	bzero(s1, 0);
	ft_bzero(s2, 0);
	print_status(s1, s2);

	printf("ft_bzero(s, len): ");
	bzero(s1, len);
	ft_bzero(s2, len);
	print_status(s1, s2);

	putchar('\n');
}
