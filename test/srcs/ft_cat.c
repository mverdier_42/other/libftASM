/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cat.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 17:46:15 by mverdier          #+#    #+#             */
/*   Updated: 2019/01/14 13:16:46 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_cat(void)
{
	int		fd;

	printf("Test ft_cat -------------------------------------------------\n\n");
	fd = open("test/srcs/main.c", O_RDONLY);
	ft_cat(fd);
	close(fd);
	ft_cat(-1);
	ft_cat(fd);
	putchar('\n');
}
