/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalnum.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 20:21:09 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/18 15:59:32 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_isalnum(void)
{
	printf("Test ft_isalnum ---------------------------------------------\n\n");
	for (int i = 0; i < 128; i++) {
		if (ft_isalnum(i) != isalnum(i)) {
			printf("KO\n\n");
			return ;
		}
	}
	printf("OK\n\n");
}
