/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalpha.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 19:29:36 by mverdier          #+#    #+#             */
/*   Updated: 2019/01/11 14:28:49 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_isalpha(void)
{
	printf("Test ft_isalpha ---------------------------------------------\n\n");
	for (int i = 0; i < 128; i++) {
		if (ft_isalpha(i) != isalpha(i)) {
			printf("KO\n\n");
			return ;
		}
	}
	printf("OK\n\n");
}
