/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isascii.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 20:36:39 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/18 16:00:26 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_isascii(void)
{
	printf("Test ft_isascii ---------------------------------------------\n\n");
	for (int i = 0; i < 128; i++) {
		if (ft_isascii(i) != isascii(i)) {
			printf("KO\n\n");
			return ;
		}
	}
	printf("OK\n\n");
}
