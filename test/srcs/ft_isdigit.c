/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isdigit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 19:29:36 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/18 15:58:17 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_isdigit(void)
{
	printf("Test ft_isdigit ---------------------------------------------\n\n");
	for (int i = 0; i < 128; i++) {
		if (ft_isdigit(i) != isdigit(i)) {
			printf("KO\n\n");
			return ;
		}
	}
	printf("OK\n\n");
}
