/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isprint.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 20:36:39 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/18 16:02:04 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_isprint(void)
{
	printf("Test ft_isprint ---------------------------------------------\n\n");
	for (int i = 0; i < 128; i++) {
		if (ft_isprint(i) != isprint(i)) {
			printf("KO\n\n");
			return ;
		}
	}
	printf("OK\n\n");
}
