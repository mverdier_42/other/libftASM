/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 17:46:15 by mverdier          #+#    #+#             */
/*   Updated: 2019/01/14 17:14:44 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	print_memchr(void *s, int c, size_t n)
{
	if (ft_memchr(s, c, n) == memchr(s, c, n))
		printf("OK\n");
	else
		printf("KO\n");
}

void	test_memchr(void)
{
	char	str[20];

	printf("Test ft_memchr ----------------------------------------------\n\n");
	strcpy(str, "Ceci est un test.");
	print_memchr(str, 't', strlen(str));
	print_memchr(str, 'C', strlen(str));
	print_memchr(str, '.', strlen(str));
	print_memchr(str, 'Z', strlen(str));
	print_memchr(str, 't', 5);
	print_memchr(str, 't', 0);
	putchar('\n');
}
