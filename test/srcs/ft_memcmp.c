/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 17:46:15 by mverdier          #+#    #+#             */
/*   Updated: 2019/01/14 17:57:20 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	print_memcmp(void *s1, void *s2, size_t n)
{
	int		res1;
	int		res2;

	if ((res1 = ft_memcmp(s1, s2, n)) == (res2 = memcmp(s1, s2, n)))
		printf("OK: %d | %d\n", res1, res2);
	else
		printf("KO: %d | %d\n", res1, res2);
}

void	test_memcmp(void)
{
	printf("Test ft_memcmp ----------------------------------------------\n\n");
	print_memcmp("Bonjour", "Bonjour", 7);
	print_memcmp("Bonjour", "BonjouR", 7);
	print_memcmp("Bonjour", "bonjour", 7);
	print_memcmp("", "", 7);
	print_memcmp("\200", "\0", 1);
	putchar('\n');
}
