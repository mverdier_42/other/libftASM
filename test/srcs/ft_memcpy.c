/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/14 14:10:43 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/14 15:48:29 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_memcpy(void)
{
	char	*s1;
	char	*s2;

	s1 = malloc(sizeof(char) * 10);
	s2 = malloc(sizeof(char) * 10);
	bzero(s1, 10);
	bzero(s2, 10);
	ft_memset(s1, '*', 9);
	ft_memset(s2, '+', 9);
	printf("Test ft_memcpy ----------------------------------------------\n\n");
	ft_memcpy(s1, s2, 4);
	printf("s1: %s\n", s1);
	printf("s1: %s\n", ft_memcpy(s1, s2, 4));
	putchar('\n');
}
