/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/14 14:10:43 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/14 15:45:15 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_memset(void)
{
	char	*str;

	str = malloc(sizeof(char) * 5);
	bzero(str, 5);
	printf("Test ft_memset ----------------------------------------------\n\n");
	ft_memset(str, '*', 4);
	printf("str: %s\n", str);
	printf("str: %s\n", ft_memset(str, -200, 4));
	//memset(str, '*', 10);
	//memset(NULL, '*', 10);
	putchar('\n');
}
