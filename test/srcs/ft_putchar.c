/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 17:46:15 by mverdier          #+#    #+#             */
/*   Updated: 2019/01/14 13:14:56 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_putchar(void)
{
	printf("Test ft_putchar ---------------------------------------------\n\n");
	for (int i = -256; i < 256; i++) {
		if (ft_putchar(i) != putchar(i)) {
			printf("\nKO\n\n");
			return ;
		}
	}
	printf("\nOK\n\n");
}
