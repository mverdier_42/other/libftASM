/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puts.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 19:29:36 by mverdier          #+#    #+#             */
/*   Updated: 2019/01/08 16:39:00 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	print_status(int ret1, int ret2)
{
	if ((ret1 > 0 && ret2 > 0) || (ret1 < 0 && ret2 < 0) || (ret1 == 0 && ret2 == 0))
		printf("OK\n\n");
	else
		printf("KO\n\n");
}

void		test_puts(void)
{
	char	hello[] = "Hello";
	int		ret1;
	int		ret2;

	printf("Test ft_puts ------------------------------------------------\n\n");

	printf("ft_puts(\"%s\"):\n", hello);
	ret1 = puts(hello);
	ret2 = ft_puts(hello);
	print_status(ret1, ret2);

	printf("ft_puts(\"\"):\n");
	ret1 = puts("");
	ret2 = ft_puts("");
	print_status(ret1, ret2);

	printf("ft_puts(NULL):\n");
	ret1 = puts(NULL);
	ret2 = ft_puts(NULL);
	print_status(ret1, ret2);
}
