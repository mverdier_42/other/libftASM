/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 19:29:36 by mverdier          #+#    #+#             */
/*   Updated: 2019/01/10 16:31:31 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_strcat(void)
{
	char	buf[9];

	printf("Test ft_strcat ----------------------------------------------\n\n");
	bzero(buf, 9);
	ft_strcat(buf, "");
	ft_strcat(buf, "Bon");
	ft_strcat(buf, "j");
	ft_strcat(buf, "our.");
	ft_strcat(buf, "");
	(strcmp(buf, "Bonjour.") == 0) ? printf("OK\n") : printf("KO\n");
	(strcmp(buf, ft_strcat(buf, "")) == 0) ? printf("OK\n") : printf("KO\n");
	putchar('\n');
}
