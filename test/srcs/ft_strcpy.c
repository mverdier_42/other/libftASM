/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 17:46:15 by mverdier          #+#    #+#             */
/*   Updated: 2019/01/14 15:38:44 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	print_status(char *str1, char *str2, char *test)
{
	if (!strcmp(ft_strcpy(str1, test), strcpy(str2, test)))
		printf("OK\n");
	else
		printf("KO\n");
}

void	test_strcpy(void)
{
	char	str1[20];
	char	str2[20];

	printf("Test ft_strcpy ----------------------------------------------\n\n");
	print_status(str1, str2, "");
	print_status(str1, str2, "abcdefghijklmnopqrs");
	print_status(str1, str2, "Reset");
	putchar('\n');
}
