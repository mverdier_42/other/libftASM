/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/14 14:10:43 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/17 17:06:26 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_strdup(void)
{
	char	*s1;
	char	*s2;
	char	*s3;

	s1 = malloc(sizeof(char) * 10);
	s2 = NULL;
	s3 = NULL;
	ft_bzero(s1, 10);
	ft_memset(s1, '*', 9);
	printf("Test ft_strdup ----------------------------------------------\n\n");
	s2 = ft_strdup(s1);
	printf("s1: %s\n", s1);
	printf("s2: %s\n", s2);
	for (size_t i = 0; i < ft_strlen(s2); i++) {
		printf("%d ", s2[i]);
	}
	putchar('\n');
	ft_memcpy(s1, "123456\0", 7);
	s3 = ft_strdup(s1);
	printf("s1: %s\n", s1);
	printf("s3: %s\n", s3);
	for (size_t i = 0; i < ft_strlen(s3); i++) {
		printf("%d ", s3[i]);
	}
	putchar('\n');
	putchar('\n');
	free(s1);
	free(s2);
	free(s3);
}
