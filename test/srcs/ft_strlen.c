/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 17:30:39 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/18 17:34:53 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_strlen(void)
{
	printf("Test ft_strlen ----------------------------------------------\n\n");

	printf("ft_strlen(\"Hello\"): ");
	if (ft_strlen("Hello") == strlen("Hello"))
		printf("OK\n");
	else
		printf("KO\n");

	printf("ft_strlen(\"\"): ");
	if (ft_strlen("") == strlen(""))
		printf("OK\n\n");
	else
		printf("KO\n\n");
}
