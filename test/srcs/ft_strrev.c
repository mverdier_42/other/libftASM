/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 17:46:15 by mverdier          #+#    #+#             */
/*   Updated: 2019/01/10 15:27:19 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_strrev(void)
{
	printf("Test ft_strrev ----------------------------------------------\n\n");
	ft_puts(ft_strrev("Hello !"));
	ft_puts(ft_strrev("*"));
	ft_puts(ft_strrev(""));
	ft_puts(ft_strrev(NULL));
	putchar('\n');
}
