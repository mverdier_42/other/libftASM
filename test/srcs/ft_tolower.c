/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tolower.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 19:29:36 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/18 16:03:49 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_tolower(void)
{
	printf("Test ft_tolower ---------------------------------------------\n\n");
	for (int i = 0; i < 128; i++) {
		if (ft_tolower(i) != tolower(i)) {
			printf("KO\n\n");
			return ;
		}
	}
	printf("OK\n\n");
}
