/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_toupper.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 19:29:36 by mverdier          #+#    #+#             */
/*   Updated: 2018/12/18 16:04:42 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	test_toupper(void)
{
	printf("Test ft_toupper ---------------------------------------------\n\n");
	for (int i = 0; i < 128; i++) {
		if (ft_toupper(i) != toupper(i)) {
			printf("KO\n\n");
			return ;
		}
	}
	printf("OK\n\n");
}
