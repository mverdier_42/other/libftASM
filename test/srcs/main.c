/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 17:21:25 by mverdier          #+#    #+#             */
/*   Updated: 2019/01/14 18:06:33 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int		main()
{
	test_bzero();
	test_isalpha();
	test_isdigit();
	test_isalnum();
	test_isascii();
	test_isprint();
	test_toupper();
	test_tolower();
	test_puts();
	test_strlen();
	test_strcat();
	test_memset();
	test_memcpy();
	test_strdup();
	test_cat();
	test_putchar();
	test_strrev();
	test_strcpy();
	test_memchr();
	test_memcmp();
	return (0);
}
